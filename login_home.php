<?php
include('login.php'); // Includes Login Script
?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="generator" content="Bootstrap Listr">
    <title>Login</title>
    <link rel="stylesheet" href="//brilliant-minds.tk/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="//vps3.wolfy1339.tk/Resources/css/font-awesome.min.css">
    <link rel="icon" type="image/png" sizes="16x16" href="http://brilliant-minds.tk/img/favicon16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://brilliant-minds.tk/img/favicon32.png">
    <link rel="icon" type="image/png" sizes="48x48" href="http://brilliant-minds.tk/img/favicon48.png">
    <link rel="icon" type="image/png" sizes="57x57" href="http://brilliant-minds.tk/img/favicon57.png">
    <link rel="icon" type="image/png" sizes="76x76" href="http://brilliant-minds.tk/img/favicon76.png">
    <link rel="icon" type="image/png" sizes="120x120" href="http://brilliant-minds.tk/img/favicon120.png">
    <link rel="icon" type="image/png" sizes="152x152" href="http://brilliant-minds.tk/img/favicon152.png">
    <link rel="apple-touch-icon" href="http://wolfy1339.tk/img/favicon.png">
    <link rel="apple-touch-icon" type="image/png" sizes="57x57" href="http://brilliant-minds.tk/img/favicon57.png">
    <link rel="apple-touch-icon" type="image/png" sizes="76x76" href="http://brilliant-minds.tk/img/favicon76.png">
    <link rel="apple-touch-icon" type="image/png" sizes="120x120" href="http://brilliant-minds.tk/img/favicon120.png">
    <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="http://brilliant-minds.tk/img/favicon152.png">
  </head>
  <body>
    <div class="container">
      <h1>PHP Login Session Example</h1>
      <form action="" method="post">
        <div class="form-group">
          <label for="name">User Name :</label>
          <input id="name" name="username" placeholder="username" type="text" class="form-control" required autofocus>
        </div>
        <div class="form-group">
          <label for="password">Password :</label>
          <input id="password" name="password" placeholder="**********" type="password" class="form-control" required>
        </div>
        <input name="submit" type="submit" value=" Login" class="btn btn-primary btn-block">
        <span><?php echo $error; ?></span>
      </form>
    </div>
  </body>
</html>
