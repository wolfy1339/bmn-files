<?php error_reporting(E_ERROR);
$ip_low = ip2long("192.30.252.0");
$ip_high = ip2long("192.30.255.255");
if (!empty($SERVER['HTTP_CLIENT_IP'])) {
    $ip = ip2long($_SERVER['HTTP_CLIENT_IP']);
} else {
    $ip = ip2long($_SERVER['REMOTE_ADDR']);
}
$CF_DO_PURGE = False;
$CF_EMAIL = file_get_contents("cf_email");
$CF_API = openssl_digest(openssl_digest(openssl_digest(file_get_contents("cloudflare"), 'sha512'), 'sha512'), 'sha512');
$CF_ZONE_ID = "f1539009d9ffaf171559ba86d48bcf17";
$CF_PURGE_URL = 'https://api.cloudflare.com/client/v4/zones/'.$CF_ZONE_ID.'/purge_cache';

if ($ip >= $ip_low && $ip <= $ip_high) {
    shell_exec("/usr/bin/git -C /var/www/BMN pull");
    // To-Do: Purge changed files from the cloudflare cache using curl
    if ($CF_DO_PURGE) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $CF_PURGE_URL);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "X-Auth-Email: ".$CF_EMAIL,
            "X-Auth-Key: ".$CF_API_KEY,
            "Content-Type: application/json"
        ));
        curl_exec($curl);
        curl_close($curl);
    }
}
else {
    include '403.php';
    header($PROTOCOL_VERSION." 403 Forbidden");
}
exit()
?>
