//Change the font of the whole website to Arial
function changeFont(){
  //Assign the <body> tag to bodyTag
  var bodyTag = document.getElementsByTagName("body");
  //Change the font of bodyTag to "Arial"
  bodyTag[0].style.fontFamily = "Arial";
}



//Change the color of the title of the group to black
function changeTitleColor(){
  //Assign the title of the group to title
  var groupTitle = document.getElementById("ctl00_ctl00_cphTemplate_menus_wrapper_title");
  //Change the font color of groupTitle to black
  groupTitle.style.color = "black";
}



//Change order of events (like "ABC has joined the group. 12:00:00 PM 01-Jan-2015") from [<Username> <Action> <Date>] to [<Date> <Action> <Date>] and make it bold
function changeEventOrder(){
  //Assign the element that contains the events to events
  var events = document.getElementsByClassName("newsFeedText");
  //Add a zero at the begining of the date (for example, "3:00:00 PM" is going to be "03:00:00 PM") and add 4 spaces between the date and the username
  for (i = 0; i < events.length; i++){
    if(events[i].getElementsByTagName("span")[0].getElementsByTagName("span")[0].innerHTML.length == 25){
      events[i].getElementsByTagName("span")[0].innerHTML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    } else {
      events[i].getElementsByTagName("span")[0].innerHTML += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      events[i].getElementsByTagName("span")[0].getElementsByTagName("span")[0].innerHTML =
        "0" + events[i].getElementsByTagName("span")[0].getElementsByTagName("span")[0].innerHTML;
      events[i].getElementsByTagName("span")[0].getElementsByTagName("span")[0].innerHTML =
        events[i].getElementsByTagName("span")[0].getElementsByTagName("span")[0].innerHTML.slice(0, 1) +
        events[i].getElementsByTagName("span")[0].getElementsByTagName("span")[0].innerHTML.slice(3);
    }
    //Change the order of the events
    events[i].insertBefore(events[i].getElementsByTagName("span")[0], events[i].getElementsByTagName("a")[0]);
    //Make events bold
    events[i].style.fontWeight = "bold";
  }
}



//Change smileys ( :), :D, :P, ;), :O, :P, B), >:(, :$, :S, :(, :'(, :|, 0:), <3 and </3 only ) Note: these smileys are from an app called kik
function changeSmileys(){
  //Assign all images in the webpage to allImages
  var allImages = document.getElementsByTagName("img");
  //Use the "allImages" variable to find smileys and change their source. All if statements in each case will change the width and height of the smiley to 19px if the smiley doesn't have a width/height attribute.
  for(i = 0; i < allImages.length; i++){
    switch(allImages[i].src){
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emsmile.gif":
        allImages[i].src = "http://i.imgur.com/h2Sjkp9.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emteeth.gif":
        allImages[i].src = "http://i.imgur.com/jYfXmHi.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emwink.gif":
        allImages[i].src = "http://i.imgur.com/uW8rWv2.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emsmileo.gif":
        allImages[i].src = "http://i.imgur.com/YzPXJZM.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emsmilep.gif":
        allImages[i].src = "http://i.imgur.com/7jbd0Bp.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emsmiled.gif":
        allImages[i].src = "http://i.imgur.com/WgP4tmR.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emangry.gif":
        allImages[i].src = "http://i.imgur.com/5rali41.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emembarrassed.gif":
        allImages[i].src = "http://i.imgur.com/EtE7wbr.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emcrook.gif":
        allImages[i].src = "http://i.imgur.com/AMrQdfj.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emsad.gif":
        allImages[i].src = "http://i.imgur.com/sc36V8S.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emcry.gif":
        allImages[i].src = "http://i.imgur.com/mDnS8zX.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emdgust.gif":
        allImages[i].src = "http://i.imgur.com/UpWAm2f.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emangel.gif":
        allImages[i].src = "http://i.imgur.com/aCAFCOA.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emlove.gif":
        allImages[i].src = "http://i.imgur.com/OdrVzO7.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
      case "http://groupbox.com/CuteSoft_Client/CuteEditor/Images/emunlove.gif":
        allImages[i].src = "http://i.imgur.com/VrUFRdq.png";
        if(allImages[i].hasAttribute("width") === false || allImages[i].hasAttribute("height") === false){
          allImages[i].width = 19;
          allImages[i].height = 19;
        }
        break;
    }
  }
}



//Change the position and the size of the WYSIWYG Editor
function changeEditorLayout(){
  //Assign the WYSIWYG editor and some other parts near it to some variables
  var wysiwygPart = document.getElementById("ctl00_ctl00_cphTemplate_BodyContent_upQuickPost");
  var pageNumPart = document.getElementById("ctl00_ctl00_cphTemplate_BodyContent_panPagingBottom");
  var checkPostPart = document.getElementById("ctl00_ctl00_cphTemplate_BodyContent_lbRefresh");
  var mainPagePart = document.getElementById("ctl00_ctl00_cphTemplate_BodyContent_panMain");
  var wysiwygEditor = document.getElementById("CE_ctl00_ctl00_cphTemplate_BodyContent_txtMessage_ID");
  var wysiwygEditorContainer = wysiwygEditor.parentNode.parentNode.parentNode;
  //Make the WYSIWYG Editor part before the page number selector part
  mainPagePart.insertBefore(wysiwygPart, pageNumPart);
  //Remove the "Check for new posts" text because it's unnecessary 
  checkPostPart.remove();
  //Change the value of the margin-left property to 0 (it's better that way)
  wysiwygEditorContainer.style.marginLeft = 0;
  //Change the size and the background image of the WYSIWYG Editor (changing the whole style attribute of the element is the only working way for some reason)
  wysiwygEditor.style =
    "background-color: rgb(244, 244, 243); border-color: rgb(221, 221, 221); border-width: 1px; border-style: solid; height: 200px; width: 1297px; background-image: url('http://i.imgur.com/gj3o9eA.png');";
}



//Modify the look of the WYSIWYG Editor
function changeEditorLook(){
  //Target some parts of the WYSIWYG Editor and make some variables equal to them
  var wysiwygEditorTopToolbar = document.getElementsByClassName("CuteEditorGroupMenu")[0];
  var resizeButton = document.getElementsByClassName("CuteEditorBottomBarContainer")[0].firstChild.firstChild.firstChild.lastChild;
  var wysiwygTextBox = document.getElementById("CE_ctl00_ctl00_cphTemplate_BodyContent_txtMessage_ID_Frame");
  var wysiwygEditorIconSeparator = document.getElementsByClassName("separator");
  //Remove the ability to resize the WYSIWYG Editor using the resize button
  resizeButton.remove();
  //Remove the background image of the top toolbar of the WYSIWYG Editor
  wysiwygEditorTopToolbar.style = "width: 1290px; border: none; background-image: url(\"\");"
  //Remove borders from the text box of the WYSIWYG Editor
  wysiwygTextBox.style = "background-color: White; border: none; height: 147px; width: 100%; display: block;";
  //Remove padding from the text box of the WYSIWYG Editor
  wysiwygTextBox.parentNode.style = "height:100%; padding: 0;";
  //Remove all line separators (the ones that devide the icons of the top toolbar of the WYSIWYG Editor)
  for(i = 0; i < wysiwygEditorIconSeparator.length; i++){
    wysiwygEditorIconSeparator[i].remove();
  }
  //Remove the final two line separators (two line separators will remain for some reason)
  wysiwygEditorIconSeparator[0].remove();
  wysiwygEditorIconSeparator[0].remove();
}



//Call all functions
changeFont();
changeTitleColor();
changeEventOrder();
chahgeSmileys();
changeEditorLayout();
changeEditorLook();
